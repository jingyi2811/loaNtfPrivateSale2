const { BN, time, expectRevert} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const LoaNTFSale = artifacts.require('LoaNTFSale');

contract('LoaNTFSale', function (accounts) {
    const [
        owner,
        whitelistAddress,
        nonWhitelistAddress,
    ] = accounts;

    const startDate = 1670025600
    const sixteen_decimal_value = '0000000000000000'

    before(async function () {
        this.loaNTFSale = await LoaNTFSale.new();
        await this.loaNTFSale.addDepositAddress(
            [whitelistAddress],
            { from: owner, value: '0', gas: '5000000' });
    });

    // it('Should not buy if date has not yet started', async function () {
    //     await time.increaseTo(startDate - 1)
    //     await this.loaNTFSale.buyImmortalSkin(1, { from: whitelistAddress, value: '1600' + sixteen_decimal_value, gas: ('5000000') });
    // })
    //
    // it('Should buy', async function () {
    //     await time.increaseTo(startDate)
    //     await this.loaNTFSale.buyImmortalSkin(1, { from: whitelistAddress, value: '1600' + sixteen_decimal_value, gas: ('5000000') });
    // })
    //
    // it('Should not buy if the address is not whitelisted', async function () {
    //     await time.increaseTo(startDate)
    //     await this.loaNTFSale.buyImmortalSkin(1, { from: nonWhitelistAddress, value: '1600' + sixteen_decimal_value, gas: ('5000000') });
    // })
    //
    // it('Should buy if the address is not whitelisted after 10 minutes', async function () {
    //     await time.increaseTo(startDate + 600)
    //     await this.loaNTFSale.buyImmortalSkin(1, { from: nonWhitelistAddress, value: '1600' + sixteen_decimal_value, gas: ('5000000') });
    // })
    //
    // it('Should not buy if qty > supply', async function () {
    //     await time.increaseTo(startDate)
    //     await this.loaNTFSale.buyImmortalSkin(51, { from: whitelistAddress, value: '1600' + sixteen_decimal_value, gas: ('5000000') });
    // })
    //
    // it('Should not buy if deposited amount is not the multiplier of quantity', async function () {
    //     await time.increaseTo(startDate)
    //     await this.loaNTFSale.buyImmortalSkin(2, { from: whitelistAddress, value: '31' + sixteen_decimal_value, gas: ('5000000') });
    // })
    //
    // it('Should not buy if date has ended', async function () {
    //     await time.increaseTo(startDate + 86400 + 1)
    //     await this.loaNTFSale.buyImmortalSkin(1, { from: whitelistAddress, value: '1600' + sixteen_decimal_value, gas: ('5000000') });
    // })

    it('Should buy', async function () {
        {
            await time.increaseTo(startDate)
            await this.loaNTFSale.buyImmortalSkin(3, {
                from: whitelistAddress,
                value: '4800' + sixteen_decimal_value,
                gas: ('5000000')
            });
            let amount = await this.loaNTFSale._itemOwned(0, whitelistAddress)
            console.log(amount.toString())

            let totalSupply = await this.loaNTFSale._itemTotalSupply(0)
            console.log(totalSupply.toString())


            let balance = await this.loaNTFSale._itemOwned(0, whitelistAddress)
            console.log(balance.toString())
        }

        {
            await time.increaseTo(startDate + 600)
            await this.loaNTFSale.buyImmortalSkin(4, {
                from: nonWhitelistAddress,
                value: '6400' + sixteen_decimal_value,
                gas: ('5000000')
            });
            let amount = await this.loaNTFSale._itemOwned(0, nonWhitelistAddress)
            console.log(amount.toString())

            let totalSupply = await this.loaNTFSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFSale._itemOwned(0, nonWhitelistAddress)
            console.log(balance.toString())
        }
    })
});
